package it.prisma.prismabookingspringdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrismaBookingSpringDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrismaBookingSpringDataApplication.class, args);
    }

}
